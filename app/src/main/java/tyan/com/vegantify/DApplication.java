package tyan.com.vegantify;

import android.app.Application;

import tyan.com.vegantify.model.DaggerRepositoryComponent;
import tyan.com.vegantify.model.RepositoryComponent;
import tyan.com.vegantify.model.modules.ContextModule;
import tyan.com.vegantify.model.modules.RepositoryModule;

public class DApplication extends Application {

    static private RepositoryComponent mRepositoryComponent;
    static public RepositoryComponent getComponent() {
        return mRepositoryComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mRepositoryComponent = DaggerRepositoryComponent.builder()
                .contextModule(new ContextModule(this))
                .repositoryModule(new RepositoryModule())
                .build();
    }
}
