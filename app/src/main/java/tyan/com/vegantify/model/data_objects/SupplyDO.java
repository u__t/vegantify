package tyan.com.vegantify.model.data_objects;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class SupplyDO extends ParentDO implements Parcelable{
    private String mIndex;
    private String mName;
    private String mCategory;

    public SupplyDO() {}

    public SupplyDO(Parcel in) {
        super(in);
        String[] data = new String[3];
        in.readStringArray(data);
        mName = data[0];
        mIndex = data[1];
        mCategory = data[2];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeStringArray(new String[] { mName, mIndex, mCategory });
    }

    public static final Parcelable.Creator<SupplyDO> CREATOR = new Parcelable.Creator<SupplyDO>() {

        @Override
        public SupplyDO createFromParcel(Parcel source) {
            return new SupplyDO(source);
        }

        @Override
        public SupplyDO[] newArray(int size) {
            return new SupplyDO[size];
        }
    };

    public String getIndex() {
        return mIndex;
    }

    public void setIndex(String mIndex) {
        this.mIndex = mIndex;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String mCategory) {
        this.mCategory = mCategory;
    }

    public String toString () {
        return String.format("%s|%s|%s|%s", getIndex(), getName(), getCategory(), getDescription());
    }

    @Override
    public String getDescription() {
        return super.getDescription();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SupplyDO supplyDO = (SupplyDO) o;
        return Objects.equals(mIndex, supplyDO.mIndex) &&
                Objects.equals(mName, supplyDO.mName) &&
                Objects.equals(mCategory, supplyDO.mCategory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mIndex, mName, mCategory);
    }
}
