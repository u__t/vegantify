package tyan.com.vegantify.model.data_objects;

import android.os.Parcel;
import android.os.Parcelable;

public class ParentDO implements Parcelable {
    private String mDescription;

    public ParentDO() {}

    public ParentDO(Parcel in) {
        String[] data = new String[1];
        in.readStringArray(data);
        mDescription = data[0];
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] { mDescription });
    }

    public static final Parcelable.Creator<ParentDO> CREATOR = new Parcelable.Creator<ParentDO>() {

        @Override
        public ParentDO createFromParcel(Parcel source) {
            return new ParentDO(source);
        }

        @Override
        public ParentDO[] newArray(int size) {
            return new ParentDO[size];
        }
    };
}
