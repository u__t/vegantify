package tyan.com.vegantify.model.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {
    private Context mContext;

    public ContextModule(Context context){
        this.mContext = context;
    }

    @Provides
    public Context context(){ return mContext.getApplicationContext(); }
}
