package tyan.com.vegantify.model.data_objects;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class IngredientsDO extends ParentDO implements Parcelable {
    private String mNameRu;
    private String mNameEn;
    private String mSynonyms;

    public IngredientsDO() {}

    public IngredientsDO(Parcel in) {
        super(in);
        String[] data = new String[3];
        in.readStringArray(data);
        mNameRu = data[0];
        mNameEn = data[1];
        mSynonyms = data[2];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeStringArray(new String[] { mNameRu, mNameEn, mSynonyms });
    }

    public static final Parcelable.Creator<IngredientsDO> CREATOR = new Parcelable.Creator<IngredientsDO>() {

        @Override
        public IngredientsDO createFromParcel(Parcel source) {
            return new IngredientsDO(source);
        }

        @Override
        public IngredientsDO[] newArray(int size) {
            return new IngredientsDO[size];
        }
    };

    public String getNameRu() {
        return mNameRu;
    }

    public void setNameRu(String mNameRu) {
        this.mNameRu = mNameRu;
    }

    public String getNameEn() {
        return mNameEn;
    }

    public void setNameEn(String mNameEn) {
        this.mNameEn = mNameEn;
    }

    public String getSynonyms() {
        return mSynonyms;
    }

    public void setSynonyms(String mSynonyms) {
        this.mSynonyms = mSynonyms;
    }

    @Override
    public String getDescription() {
        return super.getDescription();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IngredientsDO that = (IngredientsDO) o;
        return Objects.equals(mNameRu, that.mNameRu) &&
                Objects.equals(mNameEn, that.mNameEn) &&
                Objects.equals(mSynonyms, that.mSynonyms);
    }

    @Override
    public int hashCode() {

        return Objects.hash(mNameRu, mNameEn, mSynonyms);
    }
}
