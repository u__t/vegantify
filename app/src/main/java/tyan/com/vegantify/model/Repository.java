package tyan.com.vegantify.model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.inject.Inject;

import tyan.com.vegantify.model.data_objects.IngredientsDO;
import tyan.com.vegantify.model.data_objects.MaterialsDO;
import tyan.com.vegantify.model.data_objects.SupplyDO;

public class Repository {
    private Context mContext;
    private MutableLiveData<ArrayList<SupplyDO>> mSuppliesList;
    private MutableLiveData<ArrayList<MaterialsDO>> mMaterialsList;
    private MutableLiveData<ArrayList<IngredientsDO>> mIngredientsList;

    @Inject
    public Repository(Context context) {
        try {
            Log.d("myLog", "-create repository-");
            mContext = context;

            mSuppliesList = new MutableLiveData<>();
            mMaterialsList = new MutableLiveData<>();
            mIngredientsList = new MutableLiveData<>();

            makeNewSuppliesList();
            makeNewMaterialsList();
            makeNewIngredientsList();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public LiveData<ArrayList<SupplyDO>> getSupplies() {
        return mSuppliesList;
    }

    public LiveData<ArrayList<MaterialsDO>> getMaterials() {
        return mMaterialsList;
    }

    public LiveData<ArrayList<IngredientsDO>> getIngredients() {
        return mIngredientsList;
    }


    private String loadJSONFromAsset(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return json;
    }

    private IngredientsDO makeIngredientsDO(JSONObject jsonObject){
        try {
            IngredientsDO ingredientsDO = new IngredientsDO();
            ingredientsDO.setNameEn(jsonObject.getString("nameen"));
            ingredientsDO.setNameRu(jsonObject.getString("nameru"));
            ingredientsDO.setDescription(jsonObject.getString("origin"));
            ingredientsDO.setSynonyms(jsonObject.getString("synonyms"));

            return ingredientsDO;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private MaterialsDO makeMaterialsDO(JSONObject jsonObject){
        try {
            MaterialsDO materialsDO = new MaterialsDO();
            materialsDO.setNameEn(jsonObject.getString("nameen"));
            materialsDO.setNameRu(jsonObject.getString("nameru"));
            materialsDO.setDescription(jsonObject.getString("description"));

            return materialsDO;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private SupplyDO makeSupplyDO(JSONObject jsonObject){
        try {
            SupplyDO supplyDO = new SupplyDO();
            supplyDO.setIndex(jsonObject.getString("index"));
            supplyDO.setCategory(jsonObject.getString("category"));
            supplyDO.setName(jsonObject.getString("name"));
            supplyDO.setDescription(jsonObject.getString("origin"));

            Log.d("myLog", "res = " + supplyDO.toString());

            return supplyDO;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void makeNewSuppliesList() {
        try {
            ArrayList<SupplyDO> supplies = new ArrayList<>();

            JSONArray jsonArray = new JSONArray(loadJSONFromAsset(mContext, "supplements.json"));
            for(int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                supplies.add(makeSupplyDO(jsonObject));
            }
            mSuppliesList.setValue(supplies);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void makeNewIngredientsList() {
        try {
            ArrayList<IngredientsDO> ingredients = new ArrayList<>();

            JSONArray jsonArray = new JSONArray(loadJSONFromAsset(mContext, "ingredients.json"));
            for(int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                ingredients.add(makeIngredientsDO(jsonObject));
            }
            mIngredientsList.setValue(ingredients);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void makeNewMaterialsList() {
        try {
            ArrayList<MaterialsDO> materials = new ArrayList<>();

            JSONArray jsonArray = new JSONArray(loadJSONFromAsset(mContext, "materials.json"));
            for(int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                materials.add(makeMaterialsDO(jsonObject));
            }
            mMaterialsList.setValue(materials);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
