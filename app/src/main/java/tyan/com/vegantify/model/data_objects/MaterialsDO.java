package tyan.com.vegantify.model.data_objects;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class MaterialsDO extends ParentDO implements Parcelable {
    private String mNameRu;
    private String mNameEn;


    public MaterialsDO() {}

    public MaterialsDO(Parcel in) {
        super(in);
        String[] data = new String[2];
        in.readStringArray(data);
        mNameRu = data[0];
        mNameEn = data[1];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeStringArray(new String[] { mNameRu, mNameEn });
    }

    public static final Parcelable.Creator<MaterialsDO> CREATOR = new Parcelable.Creator<MaterialsDO>() {

        @Override
        public MaterialsDO createFromParcel(Parcel source) {
            return new MaterialsDO(source);
        }

        @Override
        public MaterialsDO[] newArray(int size) {
            return new MaterialsDO[size];
        }
    };

    public String getNameRu() {
        return mNameRu;
    }

    public void setNameRu(String mNameRu) {
        this.mNameRu = mNameRu;
    }

    public String getNameEn() {
        return mNameEn;
    }

    public void setNameEn(String mNameEn) {
        this.mNameEn = mNameEn;
    }

    @Override
    public String getDescription() {
        return super.getDescription();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MaterialsDO that = (MaterialsDO) o;
        return Objects.equals(mNameRu, that.mNameRu) &&
                Objects.equals(mNameEn, that.mNameEn);
    }

    @Override
    public int hashCode() {

        return Objects.hash(mNameRu, mNameEn);
    }
}
