package tyan.com.vegantify.model;

import javax.inject.Singleton;

import dagger.Component;
import tyan.com.vegantify.model.modules.ContextModule;
import tyan.com.vegantify.model.modules.RepositoryModule;

@Component(modules = {RepositoryModule.class, ContextModule.class})
@Singleton
public interface RepositoryComponent {
    Repository getRepositori();
}
