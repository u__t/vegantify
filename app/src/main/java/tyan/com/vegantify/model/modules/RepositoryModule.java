package tyan.com.vegantify.model.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import tyan.com.vegantify.model.Repository;

@Module(includes = ContextModule.class)
public class RepositoryModule {
    @Provides
    @Singleton
    public Repository getRepository(Context context) {
        return new Repository(context);
    }
}
