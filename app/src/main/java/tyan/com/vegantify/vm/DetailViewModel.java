package tyan.com.vegantify.vm;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import tyan.com.vegantify.model.data_objects.IngredientsDO;
import tyan.com.vegantify.model.data_objects.MaterialsDO;
import tyan.com.vegantify.model.data_objects.ParentDO;
import tyan.com.vegantify.model.data_objects.SupplyDO;


public class DetailViewModel extends AndroidViewModel {

    private ParentDO mDataObject;

    public DetailViewModel(@NonNull Application application) {
        super(application);
    }

    public SupplyDO getSupply() {
        return (SupplyDO) getDataObject();
    }

    public MaterialsDO getMaterails() {
        return (MaterialsDO) getDataObject();
    }

    public IngredientsDO getIngredients() {
        return (IngredientsDO) getDataObject();
    }

    public void setDataObject(ParentDO mDataObject) {
        this.mDataObject = mDataObject;
    }

    public ParentDO getDataObject() {
        return mDataObject;
    }
}
