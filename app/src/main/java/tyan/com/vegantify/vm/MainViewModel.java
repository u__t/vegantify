package tyan.com.vegantify.vm;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import tyan.com.vegantify.DApplication;
import tyan.com.vegantify.model.Repository;
import tyan.com.vegantify.model.data_objects.IngredientsDO;
import tyan.com.vegantify.model.data_objects.MaterialsDO;
import tyan.com.vegantify.model.data_objects.SupplyDO;

public class MainViewModel extends AndroidViewModel {

    private Repository mRepository;

    public MainViewModel(@NonNull Application application) {
        super(application);

        mRepository = DApplication.getComponent().getRepositori();
    }

    public LiveData<ArrayList<SupplyDO>> getSupplies() {
        return mRepository.getSupplies();
    }

    public LiveData<ArrayList<MaterialsDO>> getMaterials() {
        return mRepository.getMaterials();
    }

    public LiveData<ArrayList<IngredientsDO>> getIngredients() {
        return mRepository.getIngredients();
    }

    public List<SupplyDO> filterSupplies(String query){
        try {
            final String lowerCaseQuery = query.toLowerCase();
            final List<SupplyDO> filteredList = new ArrayList<>();
            if(null != mRepository.getSupplies().getValue()) {
                for (SupplyDO object : mRepository.getSupplies().getValue()) {
                    final String text = object.getName().toLowerCase();
                    if (text.contains(lowerCaseQuery)) {
                        filteredList.add(object);
                    }
                }
            }
            return filteredList;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<MaterialsDO> filterMaterails(String query){
        try {
            final String lowerCaseQuery = query.toLowerCase();
            final List<MaterialsDO> filteredList = new ArrayList<>();
            if(null != mRepository.getMaterials().getValue()) {
                for (MaterialsDO object : mRepository.getMaterials().getValue()) {
                    final String text = object.getNameRu().toLowerCase();
                    final String textE = object.getNameEn().toLowerCase();
                    if (text.contains(lowerCaseQuery) || textE.contains(lowerCaseQuery)) {
                        filteredList.add(object);
                    }
                }
            }
            return filteredList;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<IngredientsDO> filterIngredients(String query){
        try {
            final String lowerCaseQuery = query.toLowerCase();
            final List<IngredientsDO> filteredList = new ArrayList<>();
            if(null != mRepository.getIngredients().getValue()) {
                for (IngredientsDO object : mRepository.getIngredients().getValue()) {
                    final String text = object.getNameRu().toLowerCase();
                    final String textE = object.getNameEn().toLowerCase();
                    if (text.contains(lowerCaseQuery) || textE.contains(lowerCaseQuery)) {
                        filteredList.add(object);
                    }
                }
            }
            return filteredList;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

}
