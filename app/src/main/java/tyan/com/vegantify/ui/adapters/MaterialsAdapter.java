package tyan.com.vegantify.ui.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import tyan.com.vegantify.databinding.ViewElementMaterialBinding;
import tyan.com.vegantify.model.data_objects.MaterialsDO;
import tyan.com.vegantify.ui.OnItemClickListener;

public class MaterialsAdapter extends RecyclerView.Adapter<MaterialsAdapter.ViewHolder>{

    private OnItemClickListener listener;
    private SortedList<MaterialsDO> mSortedList = new SortedList<>(MaterialsDO.class, new SortedList.Callback<MaterialsDO>() {
        @Override
        public int compare(MaterialsDO o1, MaterialsDO o2) {
            return o1.getNameRu().compareToIgnoreCase(o2.getNameRu());
        }

        @Override
        public void onChanged(int position, int count) {
            notifyItemRangeChanged(position, count);
        }

        @Override
        public boolean areContentsTheSame(MaterialsDO oldItem, MaterialsDO newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areItemsTheSame(MaterialsDO item1, MaterialsDO item2) {
            return item1.getNameRu().equals(item2.getNameRu());
        }

        @Override
        public void onInserted(int position, int count) {
            notifyItemRangeInserted(position, count);
        }

        @Override
        public void onRemoved(int position, int count) {
            notifyItemRangeRemoved(position, count);
        }

        @Override
        public void onMoved(int fromPosition, int toPosition) {
            notifyItemMoved(fromPosition, toPosition);
        }
    });

    public MaterialsAdapter() {
    }

    public void add(MaterialsDO model) {
        mSortedList.add(model);
    }

    public void remove(MaterialsDO model) {
        mSortedList.remove(model);
    }

    public void add(List<MaterialsDO> models) {
        mSortedList.addAll(models);
    }

    public void remove(List<MaterialsDO> models) {
        mSortedList.beginBatchedUpdates();
        for (MaterialsDO model : models) {
            mSortedList.remove(model);
        }
        mSortedList.endBatchedUpdates();
    }

    public void replaceAll(List<MaterialsDO> models) {
        mSortedList.beginBatchedUpdates();
        for (int i = mSortedList.size() - 1; i >= 0; i--) {
            final MaterialsDO model = mSortedList.get(i);
            if (!models.contains(model)) {
                mSortedList.remove(model);
            }
        }
        mSortedList.addAll(models);
        mSortedList.endBatchedUpdates();
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return mSortedList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull MaterialsAdapter.ViewHolder holder, int position) {
        try {
            MaterialsDO materialsDO = mSortedList.get(position);
            holder.mBinding.setMaterial(materialsDO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public MaterialsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewElementMaterialBinding binding = ViewElementMaterialBinding.inflate(inflater, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ViewElementMaterialBinding mBinding;
        private ViewHolder (View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.getBinding(itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v){
            if(null != listener) {
                listener.onClick(mSortedList.get(getAdapterPosition()));
            }
        }
    }
}
