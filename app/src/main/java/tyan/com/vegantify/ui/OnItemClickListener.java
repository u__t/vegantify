package tyan.com.vegantify.ui;

import tyan.com.vegantify.model.data_objects.ParentDO;

public interface OnItemClickListener {
    void onClick(ParentDO object);
}
