package tyan.com.vegantify.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import tyan.com.vegantify.R;
import tyan.com.vegantify.ui.fragments.IngredientsFragment;
import tyan.com.vegantify.ui.fragments.MaterialsFragment;
import tyan.com.vegantify.ui.fragments.SupplementFragment;
import tyan.com.vegantify.vm.MainViewModel;


//todo для того что бы доделать поиск, надо прричтать вот это
//todo https://stackoverflow.com/questions/30398247/how-to-filter-a-recyclerview-with-a-searchview
public class MainActivity extends AppCompatActivity {

    public static final int DEFAULT_TAB = R.id.navigation_supplement;
    private MainViewModel mVM;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_supplement:
                    mViewPager.setCurrentItem(0);
                    return true;
                case R.id.navigation_materials:
                    mViewPager.setCurrentItem(1);
                    return true;
                case R.id.navigation_ingredients:
                    mViewPager.setCurrentItem(2);
                    return true;
            }
            return false;
        }
    };

    private ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if (mPrevMenuItem != null) {
                mPrevMenuItem.setChecked(false);
            } else {
                mBottomNavigationView.getMenu().getItem(0).setChecked(false);
            }
            Log.d("page", "onPageSelected: " + position);
            mBottomNavigationView.getMenu().getItem(position).setChecked(true);
            mPrevMenuItem = mBottomNavigationView.getMenu().getItem(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    ViewPager mViewPager;
    MenuItem mPrevMenuItem;
    BottomNavigationView mBottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBottomNavigationView = findViewById(R.id.navigation);
        mBottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mViewPager = findViewById(R.id.viewpager);
        mViewPager.addOnPageChangeListener(mOnPageChangeListener);
        setupViewPager(mViewPager);

        if (null == savedInstanceState) {
            mBottomNavigationView.setSelectedItemId(DEFAULT_TAB);
        }

        mVM = ViewModelProviders.of(this).get(MainViewModel.class);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new SupplementFragment());
        viewPagerAdapter.addFragment(new MaterialsFragment());
        viewPagerAdapter.addFragment(new IngredientsFragment());
        viewPager.setAdapter(viewPagerAdapter);
    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        return true;
    }*/
}
