package tyan.com.vegantify.ui.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tyan.com.vegantify.R;
import tyan.com.vegantify.databinding.FragmentSupplyDetailBinding;
import tyan.com.vegantify.vm.DetailViewModel;

public class DetailSupplyFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentSupplyDetailBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_supply_detail, container, false);
        View rootView = binding.getRoot();

        if(null != getActivity()){
            DetailViewModel vm = ViewModelProviders.of(getActivity()).get(DetailViewModel.class);
            binding.setVm(vm);
        }

        return rootView;
    }
}
