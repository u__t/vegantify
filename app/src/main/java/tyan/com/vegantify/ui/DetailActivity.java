package tyan.com.vegantify.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import tyan.com.vegantify.R;
import tyan.com.vegantify.model.data_objects.ParentDO;
import tyan.com.vegantify.ui.fragments.DetailIngredientFragment;
import tyan.com.vegantify.ui.fragments.DetailMaterialFragment;
import tyan.com.vegantify.ui.fragments.DetailSupplyFragment;
import tyan.com.vegantify.vm.DetailViewModel;

public class DetailActivity extends AppCompatActivity {

    static final public String DATA_TYPE_KEY = "type";
    static final public String DATA_OBJECT_KEY = "object_key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        DetailViewModel vm = ViewModelProviders.of(this).get(DetailViewModel.class);

        int type;
        ParentDO dataObject;

        Intent i = getIntent();
        if(null != i) {
            type = i.getIntExtra(DATA_TYPE_KEY, 0);
            dataObject =  i.getParcelableExtra(DATA_OBJECT_KEY);
            vm.setDataObject(dataObject);
            setFragment(type);
        }
    }

    private void setFragment(int type) {
        try {
            Fragment fragment;

            switch (type) {
                case 0:{
                    fragment = new DetailSupplyFragment();
                }break;
                case 1:{
                    fragment = new DetailMaterialFragment();
                }break;
                case 2:{
                    fragment = new DetailIngredientFragment();
                }break;
                default: {
                    fragment = new DetailSupplyFragment();
                }
            }

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.frame_id, fragment)
                    .addToBackStack(null)
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed(){
        finish();
    }
}
