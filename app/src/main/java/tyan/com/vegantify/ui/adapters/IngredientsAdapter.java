package tyan.com.vegantify.ui.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import tyan.com.vegantify.databinding.ViewElementIngredientBinding;
import tyan.com.vegantify.model.data_objects.IngredientsDO;
import tyan.com.vegantify.ui.OnItemClickListener;

public class IngredientsAdapter extends RecyclerView.Adapter<IngredientsAdapter.ViewHolder> {

    private OnItemClickListener listener;
    private SortedList<IngredientsDO> mSortedList = new SortedList<>(IngredientsDO.class, new SortedList.Callback<IngredientsDO>() {
        @Override
        public int compare(IngredientsDO o1, IngredientsDO o2) {
            return o1.getNameRu().compareToIgnoreCase(o2.getNameRu());
        }

        @Override
        public void onChanged(int position, int count) {
            notifyItemRangeChanged(position, count);
        }

        @Override
        public boolean areContentsTheSame(IngredientsDO oldItem, IngredientsDO newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areItemsTheSame(IngredientsDO item1, IngredientsDO item2) {
            return item1.getNameRu().equals(item2.getNameRu());
        }

        @Override
        public void onInserted(int position, int count) {
            notifyItemRangeInserted(position, count);
        }

        @Override
        public void onRemoved(int position, int count) {
            notifyItemRangeRemoved(position, count);
        }

        @Override
        public void onMoved(int fromPosition, int toPosition) {
            notifyItemMoved(fromPosition, toPosition);
        }
    });

    public IngredientsAdapter() {

    }

    public void add(IngredientsDO model) {
        mSortedList.add(model);
    }

    public void remove(IngredientsDO model) {
        mSortedList.remove(model);
    }

    public void add(List<IngredientsDO> models) {
        mSortedList.addAll(models);
    }

    public void remove(List<IngredientsDO> models) {
        mSortedList.beginBatchedUpdates();
        for (IngredientsDO model : models) {
            mSortedList.remove(model);
        }
        mSortedList.endBatchedUpdates();
    }

    public void replaceAll(List<IngredientsDO> models) {
        mSortedList.beginBatchedUpdates();
        for (int i = mSortedList.size() - 1; i >= 0; i--) {
            final IngredientsDO model = mSortedList.get(i);
            if (!models.contains(model)) {
                mSortedList.remove(model);
            }
        }
        mSortedList.addAll(models);
        mSortedList.endBatchedUpdates();
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return mSortedList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull IngredientsAdapter.ViewHolder holder, int position) {
        try {
            IngredientsDO ingredientsDO = mSortedList.get(position);
            holder.mBinding.setIngredient(ingredientsDO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public IngredientsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewElementIngredientBinding binding = ViewElementIngredientBinding.inflate(inflater, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ViewElementIngredientBinding mBinding;
        private ViewHolder (View itemView) {
            super(itemView);

            try {
                mBinding = DataBindingUtil.getBinding(itemView);
                itemView.setOnClickListener(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onClick(View v){
            if(null != listener) {
                listener.onClick(mSortedList.get(getAdapterPosition()));
            }
        }
    }
}
