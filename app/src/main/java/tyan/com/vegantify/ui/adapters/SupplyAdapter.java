package tyan.com.vegantify.ui.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import tyan.com.vegantify.databinding.ViewElementSupplyBinding;
import tyan.com.vegantify.model.data_objects.SupplyDO;
import tyan.com.vegantify.ui.OnItemClickListener;

public class SupplyAdapter extends RecyclerView.Adapter<SupplyAdapter.ViewHolder> {

    private OnItemClickListener listener;
    private final SortedList<SupplyDO> mSortedList = new SortedList<>(SupplyDO.class, new SortedList.Callback<SupplyDO>() {
        @Override
        public int compare(SupplyDO o1, SupplyDO o2) {
            return o1.getIndex().compareToIgnoreCase(o2.getIndex());
        }

        @Override
        public void onChanged(int position, int count) {
            notifyItemRangeChanged(position, count);
        }

        @Override
        public boolean areContentsTheSame(SupplyDO oldItem, SupplyDO newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areItemsTheSame(SupplyDO item1, SupplyDO item2) {
            return item1.getIndex().equals(item2.getIndex());
        }

        @Override
        public void onInserted(int position, int count) {
            notifyItemRangeInserted(position, count);
        }

        @Override
        public void onRemoved(int position, int count) {
            notifyItemRangeRemoved(position, count);
        }

        @Override
        public void onMoved(int fromPosition, int toPosition) {
            notifyItemMoved(fromPosition, toPosition);
        }
    });

    public SupplyAdapter() {}


    public void add(SupplyDO model) {
        mSortedList.add(model);
    }

    public void remove(SupplyDO model) {
        mSortedList.remove(model);
    }

    public void add(List<SupplyDO> models) {
        mSortedList.addAll(models);
    }

    public void remove(List<SupplyDO> models) {
        mSortedList.beginBatchedUpdates();
        for (SupplyDO model : models) {
            mSortedList.remove(model);
        }
        mSortedList.endBatchedUpdates();
    }

    public void replaceAll(List<SupplyDO> models) {
        mSortedList.beginBatchedUpdates();
        for (int i = mSortedList.size() - 1; i >= 0; i--) {
            final SupplyDO model = mSortedList.get(i);
            if (!models.contains(model)) {
                mSortedList.remove(model);
            }
        }
        mSortedList.addAll(models);
        mSortedList.endBatchedUpdates();
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return mSortedList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull SupplyAdapter.ViewHolder holder, int position) {
        try {
             SupplyDO supplyDO = mSortedList.get(position);
             holder.mBuilding.setSupply(supplyDO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public SupplyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewElementSupplyBinding binding = ViewElementSupplyBinding.inflate(inflater, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ViewElementSupplyBinding mBuilding;
        private ViewHolder (View itemView) {
            super(itemView);
            try {
                mBuilding = DataBindingUtil.bind(itemView);
                itemView.setOnClickListener(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onClick(View v){
            if(null != listener) {
                listener.onClick(mSortedList.get(getAdapterPosition()));
            }
        }
    }
}
