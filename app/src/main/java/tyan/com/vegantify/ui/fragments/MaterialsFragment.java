package tyan.com.vegantify.ui.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import tyan.com.vegantify.R;
import tyan.com.vegantify.model.data_objects.MaterialsDO;
import tyan.com.vegantify.model.data_objects.ParentDO;
import tyan.com.vegantify.ui.DetailActivity;
import tyan.com.vegantify.ui.OnItemClickListener;
import tyan.com.vegantify.ui.adapters.MaterialsAdapter;
import tyan.com.vegantify.vm.MainViewModel;

import static tyan.com.vegantify.ui.DetailActivity.DATA_OBJECT_KEY;
import static tyan.com.vegantify.ui.DetailActivity.DATA_TYPE_KEY;

public class MaterialsFragment extends Fragment implements OnItemClickListener,
        SearchView.OnQueryTextListener  {

    private MainViewModel mVM;
    private MaterialsAdapter mAdapter;
    private RecyclerView mRecyclerView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_materials, container, false);

        mRecyclerView = rootView.findViewById(R.id.materials_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);

        mAdapter = new MaterialsAdapter();
        mAdapter.setListener(this);
        mRecyclerView.setAdapter(mAdapter);

        setHasOptionsMenu(true);

        if(null != getActivity()) {
            mVM = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            mVM.getMaterials().observe(this, mAdapter::add);
        }

        return rootView;
    }

    @Override
    public void onClick(ParentDO object) {
        startDetail(object);
    }

    private void startDetail(ParentDO object) {
        try {
            Intent i = new Intent(getContext(), DetailActivity.class);
            i.putExtra(DATA_TYPE_KEY, 1);
            if(null != mVM.getMaterials().getValue()) {
                i.putExtra(DATA_OBJECT_KEY, object);
                startActivity(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.options_menu, menu);
        final MenuItem searchItem = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextChange(String query) {
        try {
            final List<MaterialsDO> filtered = mVM.filterMaterails(query);
            mAdapter.replaceAll(filtered);
            mRecyclerView.scrollToPosition(0);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }
}
